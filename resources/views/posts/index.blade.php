@extends('layouts.app')

@section('content')

    <div class="flex justify-center">
        <div class="w-11/12 bg-white p-6 rounded-lg">
            <div class="grid grid-cols-2 gap-4">
                <div>
                    <div style="font-size:28px">
                        <p>Record Sample</p>
                    </div>
                </div>
                <div class="text-right">
                    <div>
                        <button type="button" class="bg-blue-500 text-white px-4 py-2 rounded font-medium focus:outline-none openSampleModal hover:bg-green-600 hover:shadow-lg">Add Sample</button>                    
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="flex justify-center">
        <div class="w-11/12 bg-white p-6 rounded-lg mt-6">
            <div class="grid grid-cols-2 gap-4">
                <div>
                    <div style="font-size:28px">
                        <p>Sample History</p>

                        @if ($samples->count())
                            @foreach ($samples as $sample)
                                <div class="mb-8">
                                    <p class="mb-2 mt-4 text-sm">Sample Name: {{ $sample->name }}</p>
                                    <p class="mb-2 text-sm">Date Recorded: <span class="text-gray-600 text-sm">{{ $sample->created_at->toDateString() }}</span></p>
                                    <p class="mb-2 text-sm">Sample Type: {{ $sample->type }}</p>
                                    <p class="mb-2 text-sm">Unit of Measure: {{ $sample->unit }}</p>
                                    <p class="mb-2 text-sm">Sample Coordinates: Latitute:{{ $sample->slatitude }} Longitude: {{ $sample->slongitude }}</p>
                                    <a href="{{ route('deleteSample', ['id' => $sample->id]) }}" class="btn btn-default text-sm font-bold text-red-500">Delete</a>
                                </div>

                            @endforeach

                            {{ $samples->links() }}
                        @else
                            <p>There are no recorded samples...</p>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="fixed z-10 inset-0 invisible overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true" id="sampleModal">

    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">

        <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>

            @auth
                <form class="mb-4" id="addSampleForm">
                    @csrf

                        <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>

                        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">

                            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">

                                <div class="sm:items-start">

                                    <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-center">

                                        <h3 class="text-lg leading-6 font-medium text-gray-900 mt-4 mb-4" id="modal-title">

                                        Select a Field

                                        </h3>

                                        {{-- Drop Down --}}
                                        

                                            <div style="font-size:18px" class="mb-4 bg-gray-100 rounded-lg border-2 shadow-sm px-4 py-3 font-medium text-black-700 sm:mt-0 sm:w-auto sm:text-lg">
                                                
                                                <select name="selectField" id="selectField" class="form control full width input-sm bg-gray-100">
                                                @foreach ($fields as $field)
                                                    <option value="{{ $field->id }}">{{ $field->name }}</option>
                                                @endforeach
                                                
                                                </select>

                                            </div>

                                        

                                        <!-- Add Field Form -->
                                        <h3 class="text-lg leading-6 font-medium text-gray-900 mb-4 mt-4" id="modal-title">

                                        Sample Details

                                        </h3>

                                            <div class="mb-4">
                                                <label for="name" class="sr-only">Sample Name</label>
                                                <input type="text" name="name" id="name" placeholder="Enter Sample Name"
                                                class="bg-gray-100 border-2 w-full p-4 rounded-lg" value="{{old('name')}}">

                                                <div class="text-red-500 mt-2 text-sm">
                                                        <span id="nameErrorMsg"></span>
                                                </div>
                                            </div>

                                            <div class="mb-4">
                                                <label for="type" class="sr-only">Sample Type</label>
                                                <input type="text" name="type" id="type" placeholder="Enter Sample Type"
                                                class="bg-gray-100 border-2 w-full p-4 rounded-lg" value="{{old('type')}}">

                                                <div class="text-red-500 mt-2 text-sm">
                                                        <span id="typeErrorMsg"></span>
                                                </div>
                                            </div>

                                            <div class="mb-4">
                                                <label for="unit" class="sr-only">Unit of Measure</label>
                                                <input type="text" name="unit" id="unit" placeholder="Unit of Measure"
                                                class="bg-gray-100 border-2 w-full p-4 rounded-lg" value="{{old('unit')}}">

                                                <div class="text-red-500 mt-2 text-sm">
                                                        <span id="unitErrorMsg"></span>
                                                </div>
                                            </div>

                                            <div class="mt-4 mb-4">
                                                <label for="optimal" class="sr-only">Optimal Sample Level</label>
                                                <input type="text" name="optimal" id="optimal" placeholder="Optimal Sample Level"
                                                class="bg-gray-100 border-2 w-full p-4 rounded-lg" value="{{old('optimal')}}">

                                                <div class="text-red-500 mt-2 text-sm">
                                                        <span id="optimalErrorMsg"></span>
                                                </div>
                                            </div>

                                            <div class="mt-4 mb-4">
                                                <label for="value" class="sr-only">Sample Value</label>
                                                <input type="text" name="value" id="value" placeholder="Enter Sample Value"
                                                class="bg-gray-100 border-2 w-full p-4 rounded-lg" value="{{old('value')}}">

                                                <div class="text-red-500 mt-2 text-sm">
                                                        <span id="valueErrorMsg"></span>
                                                </div>
                                            </div>

                                            <h3 class="text-lg leading-6 font-medium text-gray-900">

                                                Sample Field Coordinates

                                            </h3>

                                            <div class="mt-4 mb-4">
                                                <label for="slatitude" class="sr-only">Sample Latitude</label>
                                                <input type="text" name="slatitude" id="slatitude" placeholder="Enter Sample Field Latitude"
                                                class="bg-gray-100 border-2 w-full p-4 rounded-lg" value="{{old('slatitude')}}">

                                                <div class="text-red-500 mt-2 text-sm">
                                                        <span id="slatitudeErrorMsg"></span>
                                                </div>
                                            </div>

                                            <div class="mt-4 mb-4">
                                                <label for="slongitude" class="sr-only">Sample Longitude</label>
                                                <input type="text" name="slongitude" id="slongitude" placeholder="Enter Sample Field Longitude"
                                                class="bg-gray-100 border-2 w-full p-4 rounded-lg"  value="{{old('slongitude')}}">

                                                <div class="text-red-500 mt-2 text-sm">
                                                        <span id="slongitudeErrorMsg"></span>
                                                </div>
                                            </div>



                                        
                                </div>

                            </div>

                        </div>

                        <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">

                            <button type="submit" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm">

                                Save

                            </button>

                            <button type="button" class="closeSampleModal mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">

                                Cancel

                            </button>
                        
                        </div>

                </form>

            @endauth
            <!-- End Field Form -->

        </div>

    </div>

</div>    


@endsection