<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Posty</title>
        
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body class="bg-gray-200">
        <nav class="p-6 bg-white flex justify-between mb-6">
            <ul class="flex items-center">
                <li>
                    <a href="/" class="p-3">Home</a>
                </li>
                <li>
                    <a href="{{ route('dashboard')}}" class="p-3">Dashboard</a>
                </li>
                <li>
                    <a href="{{ route('samples')}}" class="p-3">Samples</a>
                </li>
            </ul>

            <ul class="flex items-center">
                @auth
                    <li>
                        <a href="" class="p-3">{{ auth()->user()->name }}</a>
                    </li>

                    <li>
                        <form action="{{ route('logout')}}" method="post" class="p-3 inline">
                            @csrf
                            <button type="submit">Logout</button>
                        </form>
                        
                    </li>
                @endauth

                @guest
                    <li>
                        <a href="{{ route('login')}}" class="p-3">Login</a>
                    </li>

                    <li>
                        <a href="{{ route('register')}}" class="p-3">Register</a>
                    </li>
                @endguest
                
            </ul>
        </nav>
        @yield('content')

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script crossorigin="anonymous" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" src="https://code.jquery.com/jquery-3.1.0.min.js">
        </script>
        
        {{-- Google map api  --}}



        {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDFIcxruxH_A3if9uVjfHKtt--YHHAFJA&libraries=geometry&callback=initMap"></script> --}}
        <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDFIcxruxH_A3if9uVjfHKtt--YHHAFJA&libraries=visualization,geometry&callback=initMap"></script>
        <script src="https://npmcdn.com/@turf/turf/turf.min.js"></script>
        <script src="{{asset('js/script.js')}}"></script>
        <script src="{{asset('js/googlemaps.js')}}"></script>
        {{-- <script src="{{asset('js/test.js')}}"></script> --}}
        

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
        </script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

        @yield('js')
    </body>
</html>