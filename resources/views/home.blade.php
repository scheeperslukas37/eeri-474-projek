@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-6 rounded-lg">
        <div style="font-size:40px">
                <p>EERI 474: Final Year Project</p>
            </div>
            <div style="font-size:40px">
                <p>Compressed Sensing Soil and Leaf Sampling</p>
            </div>

            <div style="font-size:20px">
                <p>By Lukas Scheepers</p>
            </div>
            
        </div>
    </div>
@endsection