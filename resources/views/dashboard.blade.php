@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-11/12 bg-white p-6 rounded-lg">
            <div class="grid grid-cols-2 gap-4">
                <div>
                    <div style="font-size:28px">
                        <p>Dashboard</p>
                    </div>
                    <div style="font-size:18px">
                        <p>Map Data</p>
                    </div>
                </div>
                <div class="text-right">
                    <div>
                        <button type="button" class="bg-blue-500 text-white px-4 py-2 rounded font-medium focus:outline-none openFieldModal hover:bg-green-600 hover:shadow-lg mb-2">Add Field</button>                    
                    </div>
                    <div>
                        <button type="button" class="bg-blue-500 text-white px-4 py-2 rounded font-medium focus:outline-none openViewModal hover:bg-green-600 hover:shadow-lg">View Field</button>                    
                 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="flex justify-center mt-6">
        <div class="w-11/12 bg-white p-6 rounded-lg">
            <div id="map" class="flex justify-center">
                
            </div>
        </div>
    </div>
{{-- First Modal --}}
    <div class="fixed z-10 inset-0 invisible overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true" id="addModal">

        <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">

            <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>

                @auth
                    <form class="mb-4" id="addFieldForm">
                        @csrf

                            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>

                            <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">

                                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">

                                    <div class="sm:items-start">

                                        <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-center">

                                            <h3 class="text-lg leading-6 font-medium text-gray-900 mb-4" id="modal-title">

                                            Add a Field

                                            </h3>

                                            <!-- Add Field Form -->
                                            
                                                <div class="mb-4">
                                                    <label for="name" class="sr-only">Field Name</label>
                                                    <input type="text" name="name" id="name" placeholder="Enter Field Name"
                                                    class="bg-gray-100 border-2 w-full p-4 rounded-lg" value="{{old('name')}}">

                                                    
                                                    <div class="text-red-500 mt-2 text-sm">
                                                        <span id="nameErrorMsg"></span>
                                                    </div>
                                                    
                                                </div>

                                                <div class="mt-4 mb-4">
                                                    <label for="description" class="sr-only">Add a description to the field</label>
                                                    <input type="text" name="description" id="description" placeholder="Enter Field Description"
                                                    class="bg-gray-100 border-2 w-full p-4 rounded-lg" value="{{old('description')}}">

                                                    <div class="text-red-500 mt-2 text-sm">
                                                        <span id="descriptionErrorMsg"></span>
                                                    </div>
                                                    
                                                </div>

                                                <h3 class="text-lg leading-6 font-medium text-gray-900">

                                                    Enter Center Field Coordinates

                                                </h3>

                                                <div class="mt-4 mb-4">
                                                    <label for="clatitude" class="sr-only">Center Latitude</label>
                                                    <input type="text" name="clatitude" id="clatitude" placeholder="Enter Center Field Latitude"
                                                    class="bg-gray-100 border-2 w-full p-4 rounded-lg" value="{{old('clatitude')}}">

                                                    <div class="text-red-500 mt-2 text-sm">
                                                        <span id="clatitudeErrorMsg"></span>
                                                        
                                                    </div>
                                                </div>

                                                <div class="mt-4 mb-4">
                                                    <label for="clongitude" class="sr-only">Center Longitude</label>
                                                    <input type="text" name="clongitude" id="clongitude" placeholder="Enter Center Field Longitude"
                                                    class="bg-gray-100 border-2 w-full p-4 rounded-lg" value="{{old('clongitude')}}">
                                                        <div class="text-red-500 mt-2 text-sm">
                                                            <span id="clongitudeErrorMsg"></span>
                                                        </div>
                                                </div>

                                                {{-- Outer Coordinates --}}

                                                <h3 class="text-lg leading-6 font-medium text-gray-900">

                                                    Add Field Border Coordinates

                                                </h3>
                                                <div class="text-red-500 mt-2 text-sm">
                                                    <span id="borderCoordinateErrorMsg"></span>
                                                </div>
                                                
                                                <div class="mt-4 mb-4" id="outerCoordinates">
                                                    <div class="grid grid-cols-3 gap-4">
                                                        <div>
                                                            <label for="addLatitudeBorder[0][latitude]" class="sr-only">Center Latitude</label>
                                                            <input type="text" name="addLatitudeBorder[0][latitude]" id="addLatitudeBorder[0][latitude]" placeholder="Latitude"
                                                            class="bg-gray-100 border-2 w-full p-4 rounded-lg" value="{{old('addLatitudeBorder[0][latitude]')}}">
                                                        </div>
                                                        <div>
                                                            <label for="addLongitudeBorder[0][longitude]" class="sr-only">Center Latitude</label>
                                                            <input type="text" name="addLongitudeBorder[0][longitude]" id="addLongitudeBorder[0][longitude]" placeholder="Longitude"
                                                            class="bg-gray-100 border-2 w-full p-4 rounded-lg" value="{{old('addLongitudeBorder[0][longitude]')}}">
                                                        </div>
                                                        <div>
                                                            <button id="addBorderButton" type="button" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm">

                                                                Add Border

                                                            </button>
                                                        </div>
                                                    </div>    
                                                </div>





                                            
                                    </div>

                                </div>

                            </div>

                            <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">

                                <button type="submit" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm">

                                    Save

                                </button>

                                <button type="button" class="closeFieldModal mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">

                                    Cancel

                                </button>
                            
                            </div>

                    </form>
                @endauth
                <!-- End Field Form -->

            </div>

        </div>

    </div>
 {{-- Second Modal --}}
    <div class="fixed z-10 inset-0 invisible overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true" id="viewModal">

        <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">

            <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>

                @auth
                    <form class="mb-4" id="viewFieldForm">
                        @csrf

                            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>

                            <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">

                                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">

                                    <div class="sm:items-start">

                                        <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-center">

                                            <h3 class="text-lg leading-6 font-medium text-gray-900 mb-4" id="modal-title">

                                            Select Field

                                            </h3>

                                            {{-- Drop Down --}}
                                        

                                            <div id="chooseField" style="font-size:18px" class="mb-4 bg-gray-100 rounded-lg border-2 shadow-sm px-4 py-3 font-medium text-black-700 sm:mt-0 sm:w-auto sm:text-lg" required>
                                                
                                                <select name="selectField" id="selectField" class="custom-select form control full width input-lg bg-gray-100">
                                                <option value="0" selected="selected">---Select Field---</option>
                                                @foreach ($fields as $field)
                                                    
                                                    <option value="{{ $field->id }}">{{ $field->name }}</option>
                                                @endforeach
                                                
                                                </select>

                                            </div>

                                            <div id="chooseType" style="font-size:18px" class="mb-4 bg-gray-100 rounded-lg border-2 shadow-sm px-4 py-3 font-medium text-black-700 sm:mt-0 sm:w-auto sm:text-lg" required>
                                                
                                                <select name="selectType" id="selectType" class="custom-select form control full width input-lg bg-gray-100">                                              
                                                </select>

                                            </div>

                                            <div id="chooseYear" style="font-size:18px" class="mb-4 bg-gray-100 rounded-lg border-2 shadow-sm px-4 py-3 font-medium text-black-700 sm:mt-0 sm:w-auto sm:text-lg" required>
                                                
                                                <select name="selectYear" id="selectYear" class="custom-select form control full width input-lg bg-gray-100">                                              
                                                </select>

                                            </div>

                                            <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">

                                                <button type="submit" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm">

                                                    View

                                                </button>

                                                <button type="button" class="closeViewModal mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">

                                                    Cancel

                                                </button>
                                            
                                            </div>


                                        </div>   
     
                                    </div>

                                </div>

                            </div>

                            
                    </form>
                @endauth
                <!-- End Field Form -->

            </div>

        </div>



@endsection

