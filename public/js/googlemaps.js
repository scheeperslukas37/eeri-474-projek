let hexgrid = [];
let borderPolygon;

function initMap(){
  getData();

  google.maps.Polygon.Shape = function(point, r1, r2, r3, r4, rotation, vertexCount, strokeColour, strokeWeight, Strokepacity, fillColour, fillOpacity, opts, tilt) {
    var rot = -rotation * Math.PI / 180;
    var points = [];
    var latConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat() + 0.1, point.lng())) * 10;
    var lngConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat(), point.lng() + 0.1)) * 10;
    var step = (360 / vertexCount) || 10;
  
    var flop = -1;
    if (tilt) {
      var I1 = 180 / vertexCount;
    } else {
      var I1 = 0;
    }
    for (var i = I1; i <= 360.001 + I1; i += step) {
      var r1a = flop ? r1 : r3;
      var r2a = flop ? r2 : r4;
      flop = -1 - flop;
      var y = r1a * Math.cos(i * Math.PI / 180);
      var x = r2a * Math.sin(i * Math.PI / 180);
      var lng = (x * Math.cos(rot) - y * Math.sin(rot)) / lngConv;
      var lat = (y * Math.cos(rot) + x * Math.sin(rot)) / latConv;
  
      points.push(new google.maps.LatLng(point.lat() + lat, point.lng() + lng));
    }
    return (new google.maps.Polygon({
      paths: points,
      strokeColor: strokeColour,
      strokeWeight: strokeWeight,
      strokeOpacity: Strokepacity,
      fillColor: fillColour,
      fillOpacity: fillOpacity
    }))
  }
  
  google.maps.Polygon.RegularPoly = function(point, radius, vertexCount, rotation, strokeColour, strokeWeight, Strokepacity, fillColour, fillOpacity, opts) {
    rotation = rotation || 0;
    var tilt = !(vertexCount & 1);
    return google.maps.Polygon.Shape(point, radius, radius, radius, radius, rotation, vertexCount, strokeColour, strokeWeight, Strokepacity, fillColour, fillOpacity, opts, tilt)
  }

}

// Create Map
function createMap(latLng){
  map = new google.maps.Map(document.getElementById('map'), {
      center: latLng,
      zoom: 16,
      mapTypeId: 'satellite'

  });
}

//Marker
function createMarker(latLng, title){
  var marker = new google.maps.Marker({
      position: latLng,
      map: map,
      title: title
  });
}

async function getData(){
  const response = await fetch('/map');
  console.log(response);
  const data = await response.json();
  console.log(data);

  if(data.length === 0){
    
    map = new google.maps.Map(document.getElementById('map'), {
      center: new google.maps.LatLng(-29.567806, 24.644615),
      zoom: 6,
      mapTypeId: 'satellite'

    });

  }else{
    createMap(getLatLng(getLat(data[0]),getLng(data[0])));

    for (let i = 0; i< data.length; i++){

        createMarker(getLatLng(getLat(data[i]),getLng(data[i])), getTitle(data[i]));
    }
  }
  
  
};
function getTitle(item){
const name = document.createElement('div');
const description = document.createElement('div');
const nameDescription = document.createElement('div');

name.textContent = `${item.name}`; 
description.textContent = `${item.description}`;
nameDescription.textContent = `${item.name}: ${item.description}`;

return nameDescription.textContent;
};

function getLat(item){
const Lat = document.createElement('div');
Lat.textContent = `${item.clatitude}`;

return Lat.textContent;
};


function getLng(item){
const Lng = document.createElement('div');
Lng.textContent = `${item.clongitude}`;

return Lng.textContent;
};

function getLatLng(lat,lng){
return  new google.maps.LatLng(lat, lng);
};

// View Field
$('#viewFieldForm').on('submit', function(e){
  console.log('View field submit');
  e.preventDefault();

  $.ajax({
      url: "/dashboard/viewField",
      type: "POST",
      data: $('#viewFieldForm').serializeArray(),
      success: function(response){
          console.log('Viewed Field Successful ');
          $('#viewModal').addClass('invisible');
          viewField(response);
          // console.log("Here " + JSON.stringify(response));
      },
      error: function(response){
          console.log('View Field Failed');
          console.log(response);
      },
  });
});

function viewField(data){
  setBorder(JSON.parse(data.field.borders));
  map.panTo(new google.maps.LatLng(data.field.clatitude, data.field.clongitude));
  viewSamples(data.data);
  map.set("zoomControl", false);
  map.set("gestureHandling", "none");
  //createHexGrid(data.field.clatitude, data.field.clongitude, 10);
};

function setBorder(borders){

  // borderPolygon = new google.maps.Data.Polygon([
  //   getOuterCoords(borders),
  // ]);

  borderPolygon = new google.maps.Polygon({
    paths: getOuterCoords(borders),
    map: map,
    clickable: false,
    strokeColor: "black",
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: "#3acc0e",
    fillOpacity: 0.3,
  });

};

function getOuterCoords(borders){
  let outerCoords = [];
  $.each(borders, function(key, value){
    let coords = {
      lat: parseFloat(value.latitude.latitude), lng: parseFloat(value.longitude.longitude)
    };
    outerCoords.push(coords);
  });

  return outerCoords;
};


function viewSamples(data) {
  /* Data points defined as an array of LatLng objects */

  var aboveHeatmapData = getHeatmapData(data, true);

  console.log("List of sample data: ");
  console.log(aboveHeatmapData);
      
  var aboveHeatmap = new google.maps.visualization.HeatmapLayer({
  data: aboveHeatmapData,
  radius: 50,
  dissipating: true,
  });

  aboveHeatmap.set("gradient", aboveHeatmap.get("gradient") ? null : getAboveGradient());
  //aboveHeatmap.set("gradient", getAboveGradient());
  aboveHeatmap.setMap(map);
  

  var belowHeatmapData = getHeatmapData(data, false);

  console.log("List of sample data: ");
  console.log(belowHeatmapData);
      
  var belowHeatmap = new google.maps.visualization.HeatmapLayer({
  data: belowHeatmapData,
  radius: 50,
  dissipating: true,
  });

  belowHeatmap.set("gradient", belowHeatmap.get("gradient") ? null : getBelowGradient());
  belowHeatmap.setMap(map);
  // belowHeatmap.set("gradient", getBelowGradient());
    
};

function getHeatmapData(data, aboveOptimal){
  heatmap = [];

  for(i=0; i<data.length; i++){
    if(aboveOptimal){
      if(data[i].value >= data[i].optimal){
        sample = {
          location: new google.maps.LatLng(parseFloat(data[i].slatitude),parseFloat(data[i].slongitude)),
          weight: calculateWeight(data[i].optimal, data[i].value)
    
        };
        heatmap.push(sample);
      }
    }else{
      if(data[i].value < data[i].optimal){
        sample = {
          location: new google.maps.LatLng(parseFloat(data[i].slatitude),parseFloat(data[i].slongitude)),
          weight: calculateWeight(data[i].optimal, data[i].value)
    
        };
        heatmap.push(sample);
      }
    }
    
  }
  return heatmap;
};

function calculateWeight(optimal, value){
  const idealWeight = 0.1;

  let percentageChange = Math.abs((value - optimal) / optimal);

  return percentageChange + idealWeight;
};

function getAboveGradient(){
 return ['rgba(58, 204, 14, 0)',
 'rgba(90, 197, 0, 1)',
 'rgba(113, 190, 0, 1)',
 'rgba(132, 182, 0, 1)',
 'rgba(149, 174, 0, 1)',
 'rgba(164, 165, 0, 1)',
 'rgba(179, 156, 0, 1)',
 'rgba(192, 146, 0, 1)',
 'rgba(204, 134, 0, 1)',
 'rgba(215, 122, 0, 1)',
 'rgba(226, 108, 0, 1)',
 'rgba(235, 93, 0, 1)',
 'rgba(243, 75, 0, 1)',
 'rgba(250, 51, 0, 1)',
 'rgba(255, 0, 0, 1)',
 ];
};

function getBelowGradient(){
  return ['rgba(58, 204, 14, 0)',
  'rgba(46, 205, 42, 1)',
  'rgba(31, 205, 59, 1)',
  'rgba(7, 206, 73, 1)',
  'rgba(0, 206, 86, 1)',
  'rgba(0, 206, 97, 1)',
  'rgba(0, 207, 108, 1)',
  'rgba(0, 207, 118, 1)',
  'rgba(0, 207, 127, 1)',
  'rgba(0, 206, 136, 1)',
  'rgba(0, 206, 144, 1)',
  'rgba(0, 206, 152, 1)',
  'rgba(0, 205, 159, 1)',
  'rgba(0, 205, 165, 1)',
  'rgba(14, 204, 171, 1)',
  ];
 };


function drawHexHorisontal(point, d, polygon, radius, bearing){
  while(google.maps.geometry.poly.containsLocation(point, polygon)){
    // if(!isInsideHexgrid(point)){
      var hex1 = google.maps.Polygon.RegularPoly(point, radius, 6, 90, "#FF0000", 1, 1, "#FF0000", 0.5);

      var marker = new google.maps.Marker({
        position: point,
        map: map,
        title: String(hexgrid.length),
        visible: false,
    });

      hex1.setMap(map);

      // google.maps.event.addListener(hex1,"mouseover",function(mouseEvent){
      //   this.setOptions({fillColor: "#green"});

      //   const titleText = mouseEvent.feature.getProperty('propContainingTooltipText');

      //   if (titleText) {
      //     map.getDiv().setAttribute('title', titleText);
      //   }
      //  }); 
       
      //  google.maps.event.addListener(hex1,"mouseout",function(mouseEvent){
      //   this.setOptions({fillColor: "#green"});

      //   map.getDiv().removeAttribute('title');
      //  });

      hexgrid.push(hex1);
    // }
    point = EOffsetBearing(point, d, bearing);
  }
}

function drawHexVertical(point, d, polygon, radius, hBearing, vBearing, aBearing){
  let useABearing = false;
  while(google.maps.geometry.poly.containsLocation(point, polygon)){
    drawHexHorisontal(point, d, polygon, radius, hBearing);
    point = EOffsetBearing(point, d, (useABearing) ? aBearing : vBearing);
    useABearing = !useABearing;
  }
}

function isInsideHexgrid(point){
  let isInGrid = false;
  for (var i = 0; i < hexgrid.length; i++) {
    if (google.maps.geometry.poly.containsLocation(point, hexgrid[i])) {
      isInGrid = true;
      break;
    }
  }
  return isInGrid;
}

function createHexGrid(lat, lng, radius) {
  // === Hexagonal grid ===
  hexrad = radius;
  var point = new google.maps.LatLng(lat, lng);
  map.setCenter(point);
  var d = 2 * hexrad * Math.cos(Math.PI / 6);

  drawHexVertical(point, d, borderPolygon, hexrad, 90, 150, 210); // af regs origin
  drawHexVertical(EOffsetBearing(point, d, 210), d, borderPolygon, hexrad, 270, 210, 150); // af links no origin
  drawHexVertical(EOffsetBearing(point, d, 30), d, borderPolygon, hexrad, 90, 330, 30); // op regs no origin
  drawHexVertical(EOffsetBearing(point, d, 270), d, borderPolygon, hexrad, 270, 30, 330); // op links

}


function EOffsetBearing(point, dist, bearing) {
  var latConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat() + 0.1, point.lng())) * 10;
  var lngConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat(), point.lng() + 0.1)) * 10;
  var lat = dist * Math.cos(bearing * Math.PI / 180) / latConv;
  var lng = dist * Math.sin(bearing * Math.PI / 180) / lngConv;
  return new google.maps.LatLng(point.lat() + lat, point.lng() + lng)
}

function isInsideBorders(point){
  console.log('We are here');
  console.log(borderPolygon);

  return google.maps.geometry.poly.containsLocation(point, borderPolygon);
}



