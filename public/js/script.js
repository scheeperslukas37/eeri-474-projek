$(document).ready(function(){
    //hide the elements
    $('#selectField').val('0');
    $('#chooseType').hide();
    $('#chooseYear').hide();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //Modals
    $('.openFieldModal').on('click', function(e){
        
        console.log('modal clicked');
        $('#addModal').removeClass('invisible');
    });

    $('.closeFieldModal').on('click', function(e){

        $('#addModal').addClass('invisible');
    });

    $('.openSampleModal').on('click', function(e){
        
        console.log('modal clicked');
        $('#sampleModal').removeClass('invisible');

    });

    $('.closeSampleModal').on('click', function(e){
        $('#sampleModal').addClass('invisible');

    });

    $('.openViewModal').on('click', function(e){
        
        console.log('modal clicked');
        $('#viewModal').removeClass('invisible');
    });

    $('.closeViewModal').on('click', function(e){

        $('#viewModal').addClass('invisible');
    });
    
    // Add Field
    $('#addFieldForm').on('submit', function(e){
        console.log('Add from field submit');
        e.preventDefault();

        let name = $('#name').val();
        let description = $('#description').val();
        let clatitude = $('#clatitude').val();
        let clongitude = $('#clongitude').val();

        $.ajax({
            url: "/map",
            type: "POST",
            data: $('#addFieldForm').serializeArray(),
            success: function(response){
                console.log('Submition Successful ');
                $('#addModal').addClass('invisible');
                $('#name').val('');
                $('#description').val('');
                $('#clatitude').val('');
                $('#clongitude').val('');
                console.log(response['data']);

                location.reload(true);

            },
            error: function(response){
                console.log('Validation Failed');
                console.log(response);
                if('name' in response.responseJSON.errors){
                    $('#nameErrorMsg').text(response.responseJSON.errors.name);
                    $('#name').addClass('border-red-500');
                }else{
                    $('#nameErrorMsg').text('');
                    $('#name').removeClass('border-red-500');
                }

                if('description' in response.responseJSON.errors){
                    $('#descriptionErrorMsg').text(response.responseJSON.errors.description);
                    $('#description').addClass('border-red-500');
                }else{
                    $('#descriptionErrorMsg').text('');
                    $('#description').removeClass('border-red-500');
                }

                if('clatitude' in response.responseJSON.errors){
                    $('#clatitudeErrorMsg').text(response.responseJSON.errors.clatitude);
                    $('#clatitude').addClass('border-red-500');
                }else{
                    $('#clatitudeErrorMsg').text('');
                    $('#clatitude').removeClass('border-red-500');
                }

                if('clongitude' in response.responseJSON.errors){
                    $('#clongitudeErrorMsg').text(response.responseJSON.errors.clongitude);
                    $('#clongitude').addClass('border-red-500');
                }else{
                    $('#clongitudeErrorMsg').text('');
                    $('#clongitude').removeClass('border-red-500');
                }

                if(!'name' in response.responseJSON.errors || !'description' in response.responseJSON.errors || !'clatitude' in response.responseJSON.errors || !'clongitude' in response.responseJSON.errors){
                    $('#borderCoordinateErrorMsg').text(response.responseJSON.errors.borderCoordinate);
                    console.log('show border error');
                }else{
                    $('#borderCoordinateErrorMsg').text('');
                    console.log('dont show border error');
                }
            },
        });
    });

    // Add Sample form
    $('#addSampleForm').on('submit', function(e){
        console.log('Add from field submit');
        e.preventDefault();

        let selectField = $('#selectField').val();


        let name = $('#name').val();
        let type = $('#type').val();
        let unit = $('#unit').val();
        let optimal = $('#optimal').val();
        let value = $('#value').val();
        let slatitude = $('#slatitude').val();
        let slongitude = $('#slongitude').val();

        $.ajax({
            url: "/samples",
            type: "POST",
            data: {
                
                selectField:selectField,
                name:name,
                type:type,
                unit:unit,
                optimal:optimal,
                value:value,
                slatitude:slatitude,
                slongitude:slongitude,
            },
            success: function(response){
                console.log('Submition Successful ');
                $('#sampleModal').addClass('invisible');
                $('#name').val('');
                $('#selectField').val('');
                $('#type').val('');
                $('#unit').val('');
                $('#optimal').val('');
                $('#value').val('');
                $('#slatitude').val('');
                $('#slongitude').val('');
                console.log(response);
                location.reload(true);
            },
            error: function(response){
                console.log('Validation Failed');
                console.log(response);
                if('name' in response.responseJSON.errors){
                    $('#nameErrorMsg').text(response.responseJSON.errors.name);
                    $('#name').addClass('border-red-500');
                }else{
                    $('#nameErrorMsg').text('');
                    $('#name').removeClass('border-red-500');
                }

                if('selectField' in response.responseJSON.errors){
                    $('#selectFieldErrorMsg').text(response.responseJSON.errors.selectField);
                    $('#selectField').addClass('border-red-500');
                }else{
                    $('#selectFieldErrorMsg').text('');
                    $('#selectField').removeClass('border-red-500');
                }

                if('type' in response.responseJSON.errors){
                    $('#typeErrorMsg').text(response.responseJSON.errors.type);
                    $('#type').addClass('border-red-500');
                }else{
                    $('#typeErrorMsg').text('');
                    $('#type').removeClass('border-red-500');
                }

                if('unit' in response.responseJSON.errors){
                    $('#unitErrorMsg').text(response.responseJSON.errors.unit);
                    $('#unit').addClass('border-red-500');
                }else{
                    $('#unitErrorMsg').text('');
                    $('#unit').removeClass('border-red-500');
                }

                if('optimal' in response.responseJSON.errors){
                    $('#optimalErrorMsg').text(response.responseJSON.errors.optimal);
                    $('#optimal').addClass('border-red-500');
                }else{
                    $('#optimalErrorMsg').text('');
                    $('#optimal').removeClass('border-red-500');
                }

                if('value' in response.responseJSON.errors){
                    $('#valueErrorMsg').text(response.responseJSON.errors.value);
                    $('#value').addClass('border-red-500');
                }else{
                    $('#valueErrorMsg').text('');
                    $('#value').removeClass('border-red-500');
                }

                if('slatitude' in response.responseJSON.errors){
                    $('#slatitudeErrorMsg').text(response.responseJSON.errors.slatitude);
                    $('#slatitude').addClass('border-red-500');
                }else{
                    $('#slatitudeErrorMsg').text('');
                    $('#slatitude').removeClass('border-red-500');
                }

                if('slongitude' in response.responseJSON.errors){
                    $('#slongitudeErrorMsg').text(response.responseJSON.errors.slongitude);
                    $('#slongitude').addClass('border-red-500');
                }else{
                    $('#slongitudeErrorMsg').text('');
                    $('#slongitude').removeClass('border-red-500');
                }
            },
        });
    });

    //View Fields

    $('#selectField').on('change',function(){
        let fieldID = this.value;

        if(fieldID){   
            $('#chooseType').show();
            console.log('Getting distinct sample type');           
                $.ajax({
                    url: '/dashboard/sample/type/'+fieldID,
                    type: "GET",
                    data : {"_token":"{{ csrf_token() }}"},
                    dataType: "json",
                    success:function(data) {
                        if(data){
                            console.log(data);           
                            $('#selectType').empty();
                            $('#selectType').focus;
                            $('#selectType').append('<option value="">-- Select Sample Type --</option>');
                            $.each(data['data'], function(key, value){
                                console.log('Inserting value ' + value.type);           
                                $('select[name="selectType"]').append('<option value="'+ value.type +'">' + value.type+ '</option>');
                            });
                        }else{
                            console.log('error no data');           
                            $('#selectType').empty();
                        }
                    },
                    error: function(response){
                        console.log('Couldnt retrieve data'); 
                    },
                });
        }else{
            $('#selectType').empty();
            $('#chooseType').hide();
            console.log('Field id not valid');           

        }

    });

    $('#selectType').on('change',function(){
        let sampleType = this.value;
        let fieldID = $('#selectField').val();

        if(sampleType){   
            $('#chooseYear').show();
            console.log('Getting distinct sample type'); 
            console.log(sampleType);
            console.log(fieldID);          
                $.ajax({
                    url: '/dashboard/sample/year/' + sampleType + '/' + fieldID,
                    type: "GET",
                    data : {"_token":"{{ csrf_token() }}"},
                    dataType: "json",
                    success:function(data) {
                        console.log(data);
                        if(data){
                            console.log(data);           
                            $('#selectYear').empty();
                            $('#selectYear').focus;
                            $('#selectYear').append('<option value="">-- Select Sample Year --</option>');
                            $.each(data, function(key, value){
                                $.each(value, function(key, value){
                                    console.log('Inserting value ' + value);           
                                    $('select[name="selectYear"]').append('<option value="'+ value +'">' + value+ '</option>');
                                });
                            });
                        }else{
                            console.log('error no data');           
                            $('#selectYear').empty();
                        }
                    },
                });
        }else{
            $('#selectYear').empty();
            $('#chooseYear').hide();
            console.log('Sample type not valid');           

        }

    });

    var i = 0;
    $('#addBorderButton').on('click',function(){
        console.log($('#addLatitudeBorder').serialize());
        console.log($('#addLongitudeBorder').serialize());
        console.log('Add another border coordinate');
        ++i;
        console.log(i);
        $('#outerCoordinates').append(
        '<div class="row grid grid-cols-3 gap-4">' +
        '<div>' + 
            '<label for="addLatitudeBorder['+ i +'][latitude]" class="sr-only">Center Latitude</label>' +
            '<input type="text" name="addLatitudeBorder['+ i +'][latitude]" id="addLatitudeBorder['+ i +'][latitude]" placeholder="Latitude"' +
            'class="bg-gray-100 border-2 w-full p-4 rounded-lg">' +
        '</div>' +
        '<div>' +
            '<label for="addLongitudeBorder['+ i +'][longitude]" class="sr-only">Center Latitude</label>' +
            '<input type="text" name="addLongitudeBorder['+ i +'][longitude]" id="addLongitudeBorder['+ i +'][longitude]" placeholder="Longitude"' +
            'class="bg-gray-100 border-2 w-full p-4 rounded-lg">' +
        '</div>' +
        '<div>' +
            '<button id="deleteBorderButton" type="button" class="w-full remove-input-field mt-3 w-full inline-flex justify-center rounded-md border border-red-300 shadow-sm px-4 py-2 bg-red text-base font-medium text-red-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">' +

                'Delete Border' +

            '</button>' +
        '</div>' +
        '</div>'
        );

        $(document).on('click', '.remove-input-field', function () {
            $(this).closest('.row').remove();
        });
    });


});