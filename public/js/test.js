var map = null;
var hexgrid = [];

function initMap() {
  var myOptions = {
    center: new google.maps.LatLng(-24.715442, 27.406146),
    zoom: 17,
    mapTypeControl: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
    },
    navigationControl: true,
    mapTypeId: google.maps.MapTypeId.SATELLITE
  }
  map = new google.maps.Map(document.getElementById("map"),
    myOptions);
  createHexGrid();
  var bounds = new google.maps.LatLngBounds();
  // Seed our dataset with random locations
  for (var i = 0; i < hexgrid.length; i++) {
    var hexbounds = new google.maps.LatLngBounds();
    for (var j = 0; j < hexgrid[i].getPath().getLength(); j++) {
      bounds.extend(hexgrid[i].getPath().getAt(j));
      hexbounds.extend(hexgrid[i].getPath().getAt(j));
    }
    hexgrid[i].bounds = hexbounds;
  }
  var span = bounds.toSpan();
  var locations = [];
  for (pointCount = 0; pointCount < 50; pointCount++) {
    place = new google.maps.LatLng(Math.random() * span.lat() + bounds.getSouthWest().lat(), Math.random() * span.lng() + bounds.getSouthWest().lng());
    bounds.extend(place);
    locations.push(place);
    // var mark = new google.maps.Marker({
    //   map: map,
    //   position: place
    // });
    // bin points in hexgrid
    for (var i = 0; i < hexgrid.length; i++) {
      if (google.maps.geometry.poly.containsLocation(place, hexgrid[i])) {
        if (!hexgrid[i].contains) {
          hexgrid[i].contains = 0;
        }
        hexgrid[i].contains++
      }
    }
  }
  // add labels
  for (var i = 0; i < hexgrid.length; i++) {
    if (typeof hexgrid[i].contains == 'undefined') {
      hexgrid[i].contains = 0;
    }
    var labelText = "<div style='background-color:white'>" + hexgrid[i].contains + "</div>";


    var myOptions = {
      content: labelText,
      boxStyle: {
        border: "1px solid black",
        textAlign: "center",
        fontSize: "8pt",
        width: "20px"
      },
      disableAutoPan: true,
      pixelOffset: new google.maps.Size(-10, 0),
      position: hexgrid[i].bounds.getCenter(),
      closeBoxURL: "",
      isHidden: false,
      pane: "floatPane",
      enableEventPropagation: true
    };

    // var ibLabel = new InfoBox(myOptions);
    // ibLabel.open(map);
  }

}

function createHexGrid() {
  // === Hexagonal grid ===
  hexrad = 10;
  var point = new google.maps.LatLng(-24.715442, 27.406146);
  map.setCenter(point);
  var hex1 = google.maps.Polygon.RegularPoly(point, hexrad, 6, 90, "#000000", 1, 1, "#00ff00", 0.5);
  hex1.setMap(map);
  var d = 2 * hexrad * Math.cos(Math.PI / 6);
  hexgrid.push(hex1);
  var hex30 = google.maps.Polygon.RegularPoly(EOffsetBearing(point, d, 30), hexrad, 6, 90, "#000000", 1, 1, "#00ffff", 0.5);
  hex30.setMap(map);
  hexgrid.push(hex30);
  var hex90 = google.maps.Polygon.RegularPoly(EOffsetBearing(point, d, 90), hexrad, 6, 90, "#000000", 1, 1, "#ffff00", 0.5);
  hex90.setMap(map);
  hexgrid.push(hex90);
  var hex150 = google.maps.Polygon.RegularPoly(EOffsetBearing(point, d, 150), hexrad, 6, 90, "#000000", 1, 1, "#00ffff", 0.5);
  hex150.setMap(map);
  hexgrid.push(hex150);
  var hex210 = google.maps.Polygon.RegularPoly(EOffsetBearing(point, d, 210), hexrad, 6, 90, "#000000", 1, 1, "#ffff00", 0.5);
  hex210.setMap(map);
  hexgrid.push(hex210);
  hex270 = google.maps.Polygon.RegularPoly(EOffsetBearing(point, d, 270), hexrad, 6, 90, "#000000", 1, 1, "#ffff00", 0.5);
  hex270.setMap(map);
  hexgrid.push(hex270);
  var hex330 = google.maps.Polygon.RegularPoly(EOffsetBearing(point, d, 330), hexrad, 6, 90, "#000000", 1, 1, "#ffff00", 0.5);
  hex330.setMap(map);
  hexgrid.push(hex330);
  var hex30_2 = google.maps.Polygon.RegularPoly(EOffsetBearing(EOffsetBearing(point, d, 30), d, 90), hexrad, 6, 90, "#000000", 1, 1, "#ff0000", 0.5);
  hex30_2.setMap(map);
  hexgrid.push(hex30_2);
  var hex150_2 = google.maps.Polygon.RegularPoly(EOffsetBearing(EOffsetBearing(point, d, 150), d, 90), hexrad, 6, 90, "#000000", 1, 1, "#0000ff", 0.5);
  hex150_2.setMap(map);
  hexgrid.push(hex150_2);
  var hex90_2 = google.maps.Polygon.RegularPoly(EOffsetBearing(EOffsetBearing(point, d, 90), d, 90), hexrad, 6, 90, "#000000", 1, 1, "#00ff00", 0.5);
  hex90_2.setMap(map);
  hexgrid.push(hex90_2);

  // This Javascript is based on code provided by the
  // Community Church Javascript Team
  // http://www.bisphamchurch.org.uk/   
  // http://econym.org.uk/gmap/

  //]]>
}
google.maps.event.addDomListener(window, 'load', initMap);

// EShapes.js
//
// Based on an idea, and some lines of code, by "thetoy" 
//
//   This Javascript is provided by Mike Williams
//   Community Church Javascript Team
//   http://www.bisphamchurch.org.uk/   
//   http://econym.org.uk/gmap/
//
//   This work is licenced under a Creative Commons Licence
//   http://creativecommons.org/licenses/by/2.0/uk/
//
// Version 0.0 04/Apr/2008 Not quite finished yet
// Version 1.0 10/Apr/2008 Initial release
// Version 3.0 12/Oct/2011 Ported to v3 by Lawrence Ross

google.maps.Polygon.Shape = function(point, r1, r2, r3, r4, rotation, vertexCount, strokeColour, strokeWeight, Strokepacity, fillColour, fillOpacity, opts, tilt) {
  var rot = -rotation * Math.PI / 180;
  var points = [];
  var latConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat() + 0.1, point.lng())) * 10;
  var lngConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat(), point.lng() + 0.1)) * 10;
  var step = (360 / vertexCount) || 10;

  var flop = -1;
  if (tilt) {
    var I1 = 180 / vertexCount;
  } else {
    var I1 = 0;
  }
  for (var i = I1; i <= 360.001 + I1; i += step) {
    var r1a = flop ? r1 : r3;
    var r2a = flop ? r2 : r4;
    flop = -1 - flop;
    var y = r1a * Math.cos(i * Math.PI / 180);
    var x = r2a * Math.sin(i * Math.PI / 180);
    var lng = (x * Math.cos(rot) - y * Math.sin(rot)) / lngConv;
    var lat = (y * Math.cos(rot) + x * Math.sin(rot)) / latConv;

    points.push(new google.maps.LatLng(point.lat() + lat, point.lng() + lng));
  }
  return (new google.maps.Polygon({
    paths: points,
    strokeColor: strokeColour,
    strokeWeight: strokeWeight,
    strokeOpacity: Strokepacity,
    fillColor: fillColour,
    fillOpacity: fillOpacity
  }))
}

google.maps.Polygon.RegularPoly = function(point, radius, vertexCount, rotation, strokeColour, strokeWeight, Strokepacity, fillColour, fillOpacity, opts) {
  rotation = rotation || 0;
  var tilt = !(vertexCount & 1);
  return google.maps.Polygon.Shape(point, radius, radius, radius, radius, rotation, vertexCount, strokeColour, strokeWeight, Strokepacity, fillColour, fillOpacity, opts, tilt)
}

function EOffsetBearing(point, dist, bearing) {
  var latConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat() + 0.1, point.lng())) * 10;
  var lngConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat(), point.lng() + 0.1)) * 10;
  var lat = dist * Math.cos(bearing * Math.PI / 180) / latConv;
  var lng = dist * Math.sin(bearing * Math.PI / 180) / lngConv;
  return new google.maps.LatLng(point.lat() + lat, point.lng() + lng)
}
