<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sample extends Model
{
    use HasFactory;

    protected $fillable =[
        'user_id',
        'map_id',
        'name',
        'type',
        'unit',
        'optimal',
        'value',
        'slatitude',
        'slongitude',
    ];

    public function samples()
    {
        return $this->hasMany(Sample::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
