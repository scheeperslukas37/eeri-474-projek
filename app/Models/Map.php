<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    use HasFactory;

    protected $fillable =[
        'user_id',
        'name',
        'description',
        'clatitude',
        'clongitude',
        'borders'
    ];

    public function map()
    {
        return $this->hasMany(Map::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
