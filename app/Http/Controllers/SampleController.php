<?php

namespace App\Http\Controllers;

use App\Models\Sample;
use App\Models\Map;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class SampleController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        $samples = Sample::where('user_id', Auth::id())->paginate(3);
        //$samples = Sample::select('id','name','type','unit','optimal','value', 'slatitude', 'slongitude')->get();

        $fields = Map::select('id','name', 'clatitude', 'clongitude')->where('user_id', Auth::id())->get();


        return view('posts.index', ['samples'=> $samples, 'fields'=> $fields]);
    }

    public function store(Request $request)
    {
        //Validation
        $this->validate($request, [
            'selectField' => 'required',
            'name' => 'required|max:30',
            'type' => 'required|max:50',
            'unit' => 'required|max:20',
            'optimal' => 'required|max:15',
            'value' => 'required|max:15',
            'slatitude' => 'required|max:255',
            'slongitude' => 'required|max:255',

        ]);

        //Store Sample
        Sample::create([
            'user_id' => Auth::id(),
            'map_id' => $request->selectField,
            'name' => $request->name,
            'type' => $request->type,
            'unit' => $request->unit,
            'optimal' => $request->optimal,
            'value' => $request->value,
            'slatitude' => $request->slatitude,
            'slongitude' => $request->slongitude,

        ]);

        return response()->json(['success'=>'Successfully']);


    }
    
    public function destroy($id)
    {
        Sample::where('id', $id)->delete();

        return redirect('/samples');
    }
}