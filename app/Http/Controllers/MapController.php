<?php

namespace App\Http\Controllers;

use App\Models\Map;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class MapController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function getFields()
    {
        return Map::where('user_id', Auth::id())->get();
    }

    public function index(Request $request)
    {
        // $latval=$request->clatitude;
        // $lngval=$request->clongitude;
        // $name=$request->name;
    
        $maps = Map::select('id','name', 'clatitude', 'clongitude')->where('user_id', Auth::id())->get();

        return view('dashboard', ['maps'=> $maps]);
    }

    public function store(Request $request)
    {
        //Validation
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'clatitude' => 'required|max:255',
            'clongitude' => 'required|max:255',
            'addLatitudeBorder.*.latitude' => 'required',
            'addLongitudeBorder.*.longitude' => 'required',

        ]);

        $borders = array();

        for ($i = 0; $i < count($request->addLatitudeBorder); $i++){
            $borders[$i] = array(
                'latitude' => $request->addLatitudeBorder[$i],
                'longitude' => $request->addLongitudeBorder[$i]
            );
        };

        //Store map
        
        Map::create([
            'user_id' => Auth::id(),
            'name' => $request->name,
            'description' => $request->description,
            'clatitude' => $request->clatitude,
            'clongitude' => $request->clongitude,
            'borders' => json_encode($borders)

        ]);

        return response()->json(['success'=>'Successfully', 'data' => $borders]);



    }   
}
