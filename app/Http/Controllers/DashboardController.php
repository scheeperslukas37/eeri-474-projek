<?php

namespace App\Http\Controllers;

use App\Models\Map;
use App\Models\Sample;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {
        $fields = Map::select('id','name', 'clatitude', 'clongitude')->where('user_id', Auth::id())->get();
        $samples = Sample::where('user_id', Auth::id())->get();
        //$samples = $this->findSampleType($samples);
        return view('dashboard', compact('fields'), ['fields'=> $fields, 'samples'=>$samples]);
    }

    public function findSampleType($id)
    {
        //add desticnt('type') to where clause
        
        if($id != NULL){
            $samples = Sample::select('type')->where('map_id', $id)->where('user_id', Auth::id())->distinct()->get();
            if($samples->count()){
                return response()->json(array(
                    'data' => $samples
                ), 200);
            }else{
                return response()->json(array(
                    'Message' => 'There are no samples for this field'
                ), 204);
            }

        }else{
            return response()->json(array(
                'Message' => 'There are no samples for this field'
            ), 204);
        }
    }

    public function findSampleByType($type, $id)
    {
        //add desticnt('type') to where clause
        
        $samples = Sample::where('type', $type)->where('map_id', $id)->where('user_id', Auth::id())->get();
        if($samples->count()){
            return response()->json(array(
                'data' => $samples
            ), 200);
        }else{
            return response()->json(array(
                'Message' => 'There are no samples for ' . $type
            ), 204);
        }
        
    }

    public function findSampleYear($type, $id)
    {
        //add desticnt('type') to where clause
        $sampleYears = array();

        $samples = Sample::select('created_at')->where('type', $type)->where('map_id', $id)->where('user_id', Auth::id())->get();
        if($samples->count()){
            foreach($samples as $sample){
                //$year = new Carbon( 'created_at' );
                array_push($sampleYears, $sample->created_at->year);
            }
            return response()->json(array(
                'data' => array_unique($sampleYears)
            ), 200);
        }else{
            return response()->json(array(
                'Message' => 'There are no samples for this field'
            ), 204);
        }
        
    }

    public function viewField(Request $request){
        // Validation
        $this->validate($request, [
            'selectField' => 'required',
            'selectType' => 'required',
            'selectYear' => 'required',
        ]);

        $data = Sample::where('map_id', $request->selectField)->where('type', $request->selectType)->whereYear('created_at', $request->selectYear)->where('user_id', Auth::id())->get();
        $field = Map::where('id', $request->selectField)->where('user_id', Auth::id())->first();

        if($data->count()){
            if($field->count()){
                return response()->json(array(
                    'data' => $data,
                    'field' => $field,
                ));
            }
        }else{
            return response()->json(array(
                'Message' => 'There are no samples based on this criteria'
            ), 204);
        }
    }
}
